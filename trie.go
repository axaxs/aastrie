package aastrie

type Trie struct {
	Chr      rune
	IsWord   bool
	Parent   *Trie
	Children []*Trie
}

func (t *Trie) GetChild(chr rune) (*Trie, bool) {
	for _, node := range t.Children {
		if node.Chr == chr {
			return node, true
		}
	}
	return nil, false
}

func (t *Trie) Insert(word string) {
	curTrie := t
	for i, r := range word {
		m, ok := curTrie.GetChild(r)
		if !ok {
			m = &Trie{r, i == len(word)-1, curTrie, []*Trie{}}
			curTrie.Children = append(curTrie.Children, m)
		}
		curTrie = m
	}
}

func (t *Trie) Delete(word string) {
	curTrie := t
	wordRoot := word[:len(word)-1]
	for _,r := range wordRoot {
		m, ok := curTrie.GetChild(r)
		if !ok {
			return
		}
		curTrie = m
	}
	toDel, ok := curTrie.GetChild(rune(word[len(word)-1]))
	if !ok {
		return
	}
	newChildren := make([]*Trie, 0, len(curTrie.Children)-1)
	for _,c := range curTrie.Children {
		if c.Chr == toDel.Chr {
			continue
		}
		newChildren = append(newChildren, c)
	}
	curTrie.Children = newChildren
}
	
func (t *Trie) TrieAt(word string) (*Trie, bool) {
	curTrie := t
	for _, r := range word {
		m, ok := curTrie.GetChild(r)
		if !ok {
			return nil, false
		}
		curTrie = m
	}
	return curTrie, true
}

func (t *Trie) BuildWord() string {
	curTrie := t
	word := string(t.Chr)
	for curTrie.Parent != nil && curTrie.Parent.Chr != ' ' {
		word = string(curTrie.Parent.Chr) + word
		curTrie = curTrie.Parent
	}
	return word
}

func NewTrie() *Trie {
	return &Trie{' ', false, nil, []*Trie{}}
}
